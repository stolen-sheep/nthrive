jQuery(document).ready(function($) {

	// variables
	var win = $(window);
	var winWidth = win.outerWidth();
	var winHeight = win.outerHeight(true);
	var scrollTop = win.scrollTop();
	var hero = $('.hero');
	var hero_content = $('.hero__content');
	var hero_copy = hero_content.children();
	var hero_right;
	var overlay = $('.hero__content');
  var seSpan = $('.onscreen .section__header span');
	var target = $('.offscreen');
	var current;
	var curTop;
	var viewPort;
	var gear = $('#gear');
	var gearTrigger = $('.interactive__header');
	var index_trigger;
	var index_target;
	var index_content;
	var index_close = $('.interactive__content-close');

	// on load, set hero height to window height
	hero.height(winHeight);

	// and add class of 'onscreen' to hero content to trigger animations
	hero_content.addClass('onscreen');

	// if screen is (re)loaded below the hero don't wait for scroll to add onscreen classes
	if (scrollTop > 0) {
		target.removeClass('offscreen').addClass('onscreen');
	}

	// update window dimensions on resize
	win.on('resize', function() {
		winWidth = win.outerWidth();
		winHeight = win.outerHeight();

    if (winWidth < 1068) {
			gearTrigger.show();
      overlay.animate({
    		'right': '20%'
    	}, {
    		'duration': 600,
    		'queue': false
    	});
    } else {
      overlay.animate({
    		'right': '60%'
    	}, {
    		'duration': 1000,
    		'queue': false
    	});
    }

	});

	// update window scrollTop, viewPort, and current section on scroll
	win.on('scroll', function() {
		target = $('.offscreen');
		scrollTop = win.scrollTop();
		viewPort = scrollTop + winHeight;
		if (target.length) {
			current = target.first();
			curTop = current.offset().top;
			if (curTop <= viewPort) {
				current.removeClass('offscreen').addClass('onscreen');
				current = target.first();
			}
		}
	}); // close win.on('scroll')

	// gear animations and content display (winWidth > 768)
	if (winWidth > 768) {
		hero_right = '60%';
		gearTrigger.on({
			mouseenter: function() {
				var $this = $(this);
				index_trigger = $this.data('trigger');
				index_target = $('.gears_alt[data-target=' + index_trigger + ']');
				index_content = $(this).next('.interactive__content');
				index_target.addClass('active');
				gear.addClass('index-' + index_trigger);
			},
			mouseleave: function() {
				var $this = $(this);
				index_target.removeClass('active');
				gear.removeClass('index-' + index_trigger);
			},
			click: function() {
				$('.interactive__content').hide();
					if ( win.outerWidth() > 768) {
						gearTrigger.hide();
					}
				var index_height = index_content.height();
				index_content.fadeIn();
			}
		});
	} else {
		hero_right = '20%';
    gearTrigger.on({
			click: function() {
				var $this = $(this);
        index_content = $(this).next('.interactive__content');
        index_content.stop().slideToggle(600);
        $('.interactive__content').not(index_content).hide();
      }
    });
	} // close if winWidth > 768

	// animate hero overlay on load
	overlay.animate({
		'left': '5%'
	}, {
		'duration': 700,
		'queue': false
	}).animate({
		'right': hero_right
	}, {
		'duration': 1000,
		'queue': false
	});

	// hide int_content if user clicks the 'X'
	index_close.on('click', function(e) {
		e.preventDefault();
		$(this).parent('.interactive__content').fadeOut('fast');
		gearTrigger.fadeIn('fast');
	});

}); // close siaf, file
